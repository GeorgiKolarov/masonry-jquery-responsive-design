$('.handle').on('click', function () {
    $('nav ul').toggleClass('show');
});

$('.grid').masonry({
    itemSelector: '.grid-item',
    columnWidth: 5,
    isFitWidth: true
});